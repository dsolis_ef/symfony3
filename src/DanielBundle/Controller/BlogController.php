<?php

// src/AppBundle/Controller/BlogController.php
namespace DanielBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
class BlogController extends Controller
{
   /**
     * @Route("/blog/{page}", name="blog_list", requirements={"page": "\d+"})
     */
    public function listAction($page=1)
    {
        /*return new Response(
            '<html><body>Showing page number: '.$page.'</body></html>'
        );*/

        $viewData = array(
        "blog_entries" => array(
            array( "title" => "First post", "body" => "blahblahblah"),
            array( "title" => "Second post", "body" => "blehblehbleh"),
            array( "title" => "Third post", "body" => "blihblihblih")
        	)
    	);

    	return $this->render('blog/index.html.twig', $viewData);


    }

    /**
     * @Route("/blog/grafica", name="blog_grafica")
     */
    public function graficaAction()
    {
        /*return new Response(
            '<html><body>Showing page number: '.$page.'</body></html>'
        );*/

        $viewData = array(
        "blog_entries" => array(
            array( "title" => "Grafica", "body" => "edcrfvtgb")
        	)
    	);

    	return $this->render('blog/grafica.html.twig', $viewData);


    }

    /**
     * @Route("/blog/{title}", name="blog_read", requirements={"title": "\S+"})
     */
    public function readAction($title)
    {
        return new Response(
            '<html><body>Showing post:'.$title.'</body></html>'
        );
    }
}